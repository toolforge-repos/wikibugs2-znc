# Copyright (c) 2024 Wikimedia Foundation and contributors.
# All Rights Reserved.
#
# This file is part of Wikibugs ZNC.
#
# Wikibugs ZNC is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Wikibugs ZNC is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# Wikibugs ZNC.  If not, see <http://www.gnu.org/licenses/>.
import pathlib

import environ

env = environ.Env()
env.smart_cast = False
environ.Env.read_env(env_file=pathlib.Path.cwd() / ".env")

# == ZNC settings ==
ZNC_USER = env.str("ZNC_USER", default="wikibugs")
ZNC_NICK = env.str("ZNC_NICK", default="wikibugs")
ZNC_REALNAME = env.str(
    "ZNC_REALNAME",
    default="https://www.mediawiki.org/wiki/Wikibugs",
)
ZNC_NETWORK = env.str("ZNC_NETWORK", default="libera")
ZNC_NETWORK_SERVER = env.str(
    "ZNC_NETWORK_SERVER",
    default="irc.ipv4.libera.chat +6697",
)
ZNC_SERVICE_NAME = env.str("ZNC_SERVICE_NAME", default="znc")
IRC_PASSWORD = env.str("IRC_PASSWORD")
