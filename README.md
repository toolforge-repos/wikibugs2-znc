Wikibugs ZNC
============

[Build Service][] project creating a container running [ZNC][] used to bridge
[Wikibugs][] to IRC networks.

Deploy on Toolforge
-------------------
```
$ ssh dev.toolforge.org
$ become wikibugs
$ toolforge envvars create IRC_PASSWORD
$ toolforge build start --image-name znc \
  https://gitlab.wikimedia.org/toolforge-repos/wikibugs2-znc
$ toolforge jobs run \
  --image tool-wikibugs/znc:latest \
  --command bouncer \
  --continuous \
  --emails none \
  znc
```

A Service object is also needed in the tool's Kubernetes namespace to expose
the bouncer's local port to other Pods within the namespace. That can be setup
by loading a YAML file like the one below using `kubectl apply`.

```yaml
kind: Service
apiVersion: v1
metadata:
  name: znc
spec:
  selector:
    app.kubernetes.io/name: znc
  ports:
    - protocol: TCP
      port: 6667
      targetPort: 6667
```
The `app.kubernetes.io/name` selector's value should match the Toolforge job
name. The `name` metadata defines the namespace internal DNS name where the
Service can be reached by code in Pods such as an IRC client of some sort.

Be sure to check `kubectl describe quota` to see if your tool has available
quota for a new Service. If it does not, see [Toolforge (Quota-requests)][]
for information on how to request additional quota.

License
-------
Licensed under the [GPL-3.0-or-later][] license. See [COPYING][] for the full
license.

[Build Service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
[ZNC]: https://wiki.znc.in/ZNC
[Wikibugs]: https://www.mediawiki.org/wiki/Wikibugs
[Toolforge (Quota-requests)]: https://phabricator.wikimedia.org/project/view/4834/
[GPL-3.0-or-later]: https://www.gnu.org/licenses/gpl-3.0.html
[COPYING]: COPYING
